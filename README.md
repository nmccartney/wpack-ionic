#Wolfpack hybrid app
---
This is a ionic application

##Getting started
---

install nodejs modules
		
		npm install
		
install bower components

		bower install
		
serve application

		sudo ionic serve
		
		
##Dependencies
Nodejs

		brew install nodejs
Ionic

		npm install -g ionic
		
Bower

		npm install -g bower
		
Gulp
		
		npm install -g gulp
		
		