/*global describe, beforeEach, it, expect, inject, module*/
'use strict';

describe('addWolf', function () {
  var scope
    , element;

  beforeEach(module('pack', 'pack/add-wolf-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<add-wolf></add-wolf>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$digest();
    expect(element.isolateScope().addWolf.name).toEqual('addWolf');
  });

});
