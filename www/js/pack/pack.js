(function () {
  'use strict';

  /* @ngdoc object
   * @name pack
   * @requires $stateProvider
   *
   * @description
   *
   */
  angular
    .module('pack', [
      'ui.router',
      'wolf',
      'uiGmapgoogle-maps',
      'map',
      'settings'
    ]);

  angular
    .module('pack')
    .config(config);

  function config($stateProvider,$ionicConfigProvider,uiGmapGoogleMapApiProvider) {


      $ionicConfigProvider.views.maxCache(1);

    $stateProvider
      .state('admin.packs', {
        url: '/packs',
        views:{
            "packs-tab":{
                templateUrl: 'js/pack/pack.tpl.html',
                controller: 'PackCtrl as packList'
            }
        }
      })
      .state('admin.view', {
        url: '/packs/:id',
        //url: '/packs/facts',
        views:{
            "packs-tab":{
                templateUrl: 'js/pack/pack-view.tpl.html',
                controller: 'PackViewCtrl as packView'
            }
        }
      })
      .state('admin.packs.view.settings', {
        url: '/settings',
        templateUrl: 'js/pack/settings.tpl.html',
        controller: 'SettingsCtrl as settings'
      });
  }

})();
