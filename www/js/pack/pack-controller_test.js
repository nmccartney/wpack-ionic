/*global describe, beforeEach, it, expect, inject, module*/
'use strict';

describe('PackCtrl', function () {
  var ctrl;

  beforeEach(module('pack'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('PackCtrl');
  }));

  it('should have ctrlName as PackCtrl', function () {
    expect(ctrl.ctrlName).toEqual('PackCtrl');
  });

});
