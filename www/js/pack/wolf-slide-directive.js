(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name pack.directive:packList
     * @restrict EA
     * @element
     *
     * @description
     *
     * @example
     <example module="pack">
     <file name="index.html">
     <pack-list></pack-list>
     </file>
     </example>
     *
     */
    angular
        .module('pack')
        .directive('wolfSlide', wolfSlide);

    function wolfSlide( PackApi, $ionicModal) {
        return {
            restrict: 'A',
            //scope: {
            //    packs:'=',
            //    selfLoad:'='
            //},
            //templateUrl: 'js/pack/pack-list-directive.tpl.html',
            replace: false,
            controllerAs: 'wolfSlide',
            controller: function ($scope ,$ionicModal) {
                var vm = this;
                vm.name = 'wolfSlide';
                vm.selfLoad = $scope.selfLoad;

                vm.openModal = function() {
                    $ionicModal.fromTemplateUrl('js/pack/pack-invite-modal.tpl.html', {
                        scope: $scope,
                        animation: 'slide-in-up'
                    }).then(function(modal) {
                        vm.modal = modal;
                        vm.modal.show();
                    });
                };

                vm.closeModal = function() {
                    vm.modal.hide();
                    vm.modal = null;
                };

            },
            link: function (scope, element, attrs) {
                /*jshint unused:false */

                if(scope.selfLoad){
                    scope.packList.getPacks();
                }

                scope.$on('pack.list.loaded',function(e,r){
                    console.log('packs loaded', e);
                });

                //console.log(scope)

                scope.$on('pack.new',function(e,r){
                    scope.packs.push(r);
                });
            }
        };
    }

})();
