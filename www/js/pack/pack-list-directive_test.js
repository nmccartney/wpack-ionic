/*global describe, beforeEach, it, expect, inject, module*/
'use strict';

describe('packList', function () {
  var scope
    , element;

  beforeEach(module('pack', 'pack/pack-list-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<pack-list></pack-list>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$digest();
    expect(element.isolateScope().packList.name).toEqual('packList');
  });

});
