(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name pack.controller:SettingsCtrl
   *
   * @description
   *
   */
  angular
    .module('pack')
    .controller('SettingsCtrl', SettingsCtrl);

  function SettingsCtrl() {
    var vm = this;
    vm.ctrlName = 'SettingsCtrl';
  }

})();
