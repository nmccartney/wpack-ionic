/*global describe, beforeEach, it, expect, inject, module*/
'use strict';

describe('createPack', function () {
  var scope
    , element;

  beforeEach(module('pack', 'pack/create-pack-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<create-pack></create-pack>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$digest();
    expect(element.isolateScope().createPack.name).toEqual('createPack');
  });

});
