(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name pack.directive:createPack
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module="pack">
       <file name="index.html">
        <create-pack></create-pack>
       </file>
     </example>
   *
   */
  angular
    .module('pack')
    .directive('createPack', createPack);

  function createPack(PackApi) {
    return {
      restrict: 'EA',
      scope: {},
      templateUrl: 'pack/create-pack-directive.tpl.html',
      replace: false,
      controllerAs: 'createPack',
      controller: function ($rootScope) {
        var vm = this;
        vm.name = 'createPack';

        vm.create = function(){
            //TODO: SHOULD VALIDATE FIRST

            PackApi.create(vm.pack)
                .then(function(r){
                    console.log('pack - ',r);
                    $rootScope.$broadcast('pack.new', r );
                });
        };

      },
      link: function (scope, element, attrs) {
        /*jshint unused:false */

        scope.$on('pack.list.new',function(e,r){
          scope.createPack.pack.name = '';
          scope.createPack.message = r.name + ' pack created';
          //console.log('got pack', scope.createPack.pack);
          //console.log('got resp', r);

        });
      }
    };
  }

})();
