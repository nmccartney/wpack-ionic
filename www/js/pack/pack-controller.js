(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name pack.controller:PackCtrl
   *
   * @description
   *
   */
  angular
    .module('pack')
    .controller('PackCtrl', PackCtrl);

  function PackCtrl( $state, PackApi,  $ionicLoading) {
    var vm = this;
    vm.ctrlName = 'PackCtrl';


      $ionicLoading.show({
          template: '<ion-spinner icon="android"></ion-spinner>'
      });

    vm.getPacks = function(){
      PackApi.list()
          .then(function(r){
              $ionicLoading.hide();
              console.log('pack list - ', r);
              vm.packs = r;
              //vm.$broadcast('pack.list.loaded');
          });
    };



      vm.delete = function(pack){

          $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
          });

          PackApi.delete(pack)
            .then(function(r){
                  $ionicLoading.hide();
              //$state.go('wolfpack',r);
            });
    };

      vm.getPacks();

  }

})();
