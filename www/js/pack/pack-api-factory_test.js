/*global describe, beforeEach, it, expect, inject, module*/
'use strict';

describe('PackApi', function () {
  var factory;

  beforeEach(module('pack'));

  beforeEach(inject(function (PackApi) {
    factory = PackApi;
  }));

  it('should have someValue be PackApi', function () {
    expect(factory.someValue).toEqual('PackApi');
  });

  it('should have someMethod return PackApi', function () {
    expect(factory.someMethod()).toEqual('PackApi');
  });

});
