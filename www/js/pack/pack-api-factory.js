(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name pack.factory:PackApi
   *
   * @description
   *
   */
  angular
    .module('pack')
    .factory('PackApi', PackApi);

  function PackApi($rootScope,$http) {

      var api = 'http://wolfpack.io';
      var dev_api = 'http://localhost:3005';

    var PackApiBase = {};

    PackApiBase.someValue = 'PackApi';

    PackApiBase.someMethod = function someMethod() {
      return 'PackApi';
    };


    /* -------- OTHER FUNCTIONS ------ */

    /**
     *
     * @param pack
     * @param wolf
     * @returns {*|webdriver.promise.Promise}
     */
    PackApiBase.addWolf = function(pack, wolf){
      return $http.post(api + '/packs/join', {
          params:{
            pack: pack,
            wolf: wolf
          }
        })
        .then(function(r){
          return r.data;
        });
    };

    /**
     *
     * @param pack
     * @param wolf
     * @returns {*|webdriver.promise.Promise}
     */
    PackApiBase.removeWolf = function (pack, wolf){
      return $http.delete(api + '/packs/remove', {
          params:{
            pack: pack.id,
            wolf: wolf.id
          }
        })
        .then(function(r){
          return r.data;
        });
    };

    PackApiBase.addWolf = function ( packId, connectorId, type ) {
      //TODO: create a invite
      return $http.get( api + '/invites/create.json' , {
        params:{
          invitedId: connectorId,
          invitorId: packId,
          type: type
        }
      });
    };



    /* ------- BASIC CRUD OPERATIONS ------- */

    /**
     *
     * @param pack
     * @returns {*|webdriver.promise.Promise}
     */
    PackApiBase.create = function(pack, wolf){
      return $http.post(api + '/packs/create',  {
            pack:pack,
            wolf:wolf
        })
        .then(function(r){
              $rootScope.$broadcast('pack.new',r.data);
          return r.data;
        });
    };

    /**
     *
     * @param pack
     * @returns {*|webdriver.promise.Promise}
     */
    PackApiBase.show = function(pack){
      return $http.get(api + '/packs/show/'+ pack, { id:pack })
        .then(function(r){
              $rootScope.$broadcast('pack.show',r.data);
          return r.data;
        });
    };

    PackApiBase.list = function(wolf){
      return $http.get(api + '/packs/', wolf)
        .then(function(r){
          $rootScope.$broadcast('pack.list',r.data);
          return r.data;
        });
    };

    /**
     *
     * @param pack
     * @returns {*|webdriver.promise.Promise}
     */
    PackApiBase.update = function(pack){
      return $http.post(api + '/packs/update', pack)
        .then(function(r){
          $rootScope.$broadcast('pack.update',r.data);
          return r.data;
        });
    };

    /**
     *
     * @param pack
     * @returns {*|webdriver.promise.Promise}
     */
    PackApiBase.delete = function(pack){
      return $http.delete(api + '/packs/destroy?id=' + pack.id)
        .then(function(r){
          $rootScope.$broadcast('pack.remove',r.data);
          return r.data;
        });
    };


    return PackApiBase;
  }

})();
