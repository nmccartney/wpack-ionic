(function () {
    'use strict';

    /**
     * @ngdoc object
     * @name pack.controller:PackCtrl
     *
     * @description
     *
     */
    angular
        .module('pack')
        .controller('PackInviteCtrl', PackInviteCtrl);

    function PackInviteCtrl($scope, PackApi, WolfApi, $ionicLoading) {
        var vm = this;
        vm.ctrlName = 'PackInviteCtrl';

        vm.sendInvite = function(pack,user){


            console.log('Add user - ',user);

            PackApi.addWolf(pack.pack.id,user.id,"Pack")
                .then(function(data){
                    console.log("Pack sending invite :: " , data);

                    var id = _.indexOf(vm.wolves, user);
                    if(id)vm.wolves.splice(id,1);
                });
        }

        vm.loadMembers = function(){
            WolfApi.list()
                .then(function(e){
                    vm.wolves = e;
                });
        }
    }

})();
