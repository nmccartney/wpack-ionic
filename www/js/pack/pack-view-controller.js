(function () {
    'use strict';

    /**
     * @ngdoc object
     * @name pack.controller:PackCtrl
     *
     * @description
     *
     */
    angular
        .module('pack')
        .controller('PackViewCtrl', PackViewCtrl);

    function PackViewCtrl( $rootScope, $state, $location, PackApi, $ionicLoading, $ionicNavBarDelegate,$ionicSlideBoxDelegate) {
        var vm = this;
        vm.ctrlName = 'PackViewCtrl';
        $location.hash('map');

        vm.state = $location.hash();


        vm.gotoMap = function($event){
            $location.hash('map');
            $ionicSlideBoxDelegate.slide(0);
        }

        vm.gotoChat = function($event){
            $location.hash('chat');
            $ionicSlideBoxDelegate.slide(1);
        }

        vm.gotoEvents = function($event){
            $location.hash('events');
            $ionicSlideBoxDelegate.slide(2);
        }

        vm.gotoWolves = function($event){
            $location.hash('wolves');
            $ionicSlideBoxDelegate.slide(3);
        }

        vm.gotoSettings = function($event){
            $location.hash('settings');
            $ionicSlideBoxDelegate.slide(4);
        }

        vm.slideChanged = function(slide) {

            var navElem = angular.element(document.querySelector('#pack-nav')).children().children()[slide];
            var slideElem = angular.element(document.querySelector('#pack-nav-slides')).children().children()[slide];
            var stateTitle = angular.element(navElem).text().trim();

            $location.hash(stateTitle)
            vm.state = $location.hash();
            $rootScope.$broadcast('pack.'+vm.state+'.load');


            angular.element(document.querySelector('#pack-nav .tab-item.active')).removeClass('active');
            angular.element(navElem).addClass('active');

            if ( slide == 0) {
                console.log(vm.currentSlide);
                $ionicSlideBoxDelegate.enableSlide(false);
            }else{
                $ionicSlideBoxDelegate.enableSlide(true);
            }

        };

        $ionicLoading.show({
            template: '<ion-spinner icon="android"></ion-spinner>'
        });

        PackApi.show($state.params.id)
            .then(function(r){
                $ionicLoading.hide();

                vm.pack = r;
                vm.setNavTitle(vm.pack.name);

                $rootScope.$broadcast('pack.loaded',vm.pack);
            });

        vm.setNavTitle = function(title) {
            $ionicNavBarDelegate.title(title);
            $ionicSlideBoxDelegate.enableSlide(false);
            //console.log(title, vm.pack);
            setTimeout(function(){$ionicNavBarDelegate.title(title)},400)
        }
        setTimeout(function(){vm.setNavTitle('Pack')},20);
    }

})();
