(function () {
    'use strict';

    /**
     * @ngdoc object
     * @name pack.controller:PackCtrl
     *
     * @description
     *
     */
    angular
        .module('pack')
        .controller('PackCreateModalCtrl', PackCreateModalCtrl);

    function PackCreateModalCtrl($scope, PackApi,  $ionicLoading) {
        var vm = this;
        vm.ctrlName = 'PackCreateModalCtrl';

        vm.createPack = function(pack){

            $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner>'
            });

            PackApi.create(pack)
                .then(function(data){

                    console.log("Pack create :: " , data);

                    vm.event = null;

                    $ionicLoading.hide();

                    //TODO: NEEDS TO BE MORE FLEXIBLE??!
                    if($scope.$parent.$parent.packListDir)
                        $scope.$parent.$parent.packListDir.modal.hide();
                    if($scope.$parent.$parent.$parent.user)
                        $scope.$parent.$parent.$parent.modal.hide();
                });
        }
    }

})();
