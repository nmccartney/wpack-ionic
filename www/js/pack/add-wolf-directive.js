(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name pack.directive:addWolf
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module="pack">
       <file name="index.html">
        <add-wolf wolves=''></add-wolf>
       </file>
     </example>
   *
   */
  angular
    .module('pack')
    .directive('addWolf', addWolf);

  function addWolf($rootScope, PackApi) {
    return {
      restrict: 'EA',
      scope: {
        wolves:'=',
        pack: '='
      },
      templateUrl: 'pack/add-wolf-directive.tpl.html',
      replace: false,
      controllerAs: 'addWolf',
      controller: function () {
        /*jshint unused:false */
        var vm = this;
        vm.name = 'addWolf';

      },
      link: function (scope, element, attrs ) {
        /*jshint unused:false */

        console.log('dir', PackApi);

        scope.invite = function( wolf ){
          console.log('dir', scope.pack.pack);
          PackApi.addWolf(scope.pack.pack.id, wolf.id, 'Pack')
            .then( function (r) {

              console.log('got join' , r);
              // get index of current list
              //var index = scope.findWolves.strayWolves.indexOf(wolf);
              // remove from dom
              //scope.findWolves.strayWolves.splice(index,1);

              $rootScope.$broadcast('pack.invite.added', r.data);
            },function(err){
              console.log('logging - ', err);
            });
        };
      }
    };
  }

})();
