(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name pack.directive:packList
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module="pack">
       <file name="index.html">
        <pack-list></pack-list>
       </file>
     </example>
   *
   */
  angular
    .module('pack')
    .directive('packList', packList);

  function packList( PackApi, $ionicModal) {
    return {
      restrict: 'EA',
      scope: {
          packs:'=',
          selfLoad:'='
      },
      templateUrl: 'js/pack/pack-list-directive.tpl.html',
      replace: false,
      controllerAs: 'packListDir',
      controller: function ($scope ,$ionicModal) {
        var vm = this;
        vm.name = 'packList';
        vm.selfLoad = $scope.selfLoad;
        vm.packs = $scope.packs;


        vm.getPacks = function(){
          PackApi.list()
              .then(function(r){
                  console.log('pack list - ', r);
                  vm.packs = r;
                  $scope.$broadcast('pack.list.loaded');
              });
        };

        $ionicModal.fromTemplateUrl('js/pack/pack-create-modal.tpl.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          vm.modal = modal;
        });

        vm.openModal = function() {
          vm.modal.show();
        };

        vm.closeModal = function() {
          vm.modal.hide();
        };

      },
      link: function (scope, element, attrs) {
        /*jshint unused:false */

          if(scope.selfLoad){
              scope.packList.getPacks();
          }




        scope.$on('pack.list.loaded',function(e,r){
          console.log('packs loaded', e);
        });

          //console.log(scope)

        scope.$on('pack.new',function(e,r){
          scope.packs.push(r);
        });
      }
    };
  }

})();
