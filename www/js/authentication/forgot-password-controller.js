(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name authentication.controller:ForgotPasswordCtrl
   *
   * @description
   *
   */
  angular
    .module('authentication')
    .controller('ForgotPasswordCtrl', ForgotPasswordCtrl);

  function ForgotPasswordCtrl() {
    var vm = this;
    vm.ctrlName = 'ForgotPasswordCtrl';
  }

})();
