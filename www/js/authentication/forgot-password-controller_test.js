/*global describe, beforeEach, it, expect, inject, module*/
'use strict';

describe('ForgotPasswordCtrl', function () {
  var ctrl;

  beforeEach(module('authentication'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('ForgotPasswordCtrl');
  }));

  it('should have ctrlName as ForgotPasswordCtrl', function () {
    expect(ctrl.ctrlName).toEqual('ForgotPasswordCtrl');
  });

});
