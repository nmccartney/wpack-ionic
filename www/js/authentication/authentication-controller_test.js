/*global describe, beforeEach, it, expect, inject, module*/
'use strict';

describe('AuthenticationCtrl', function () {
  var ctrl;

  beforeEach(module('authentication'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('AuthenticationCtrl');
  }));

  it('should have ctrlName as AuthenticationCtrl', function () {
    expect(ctrl.ctrlName).toEqual('AuthenticationCtrl');
  });

});
