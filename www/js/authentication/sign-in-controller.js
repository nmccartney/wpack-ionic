(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name authentication.controller:SignInCtrl
   *
   * @description
   *
   */
  angular
    .module('authentication')
    .controller('SignInCtrl', SignInCtrl);

  function SignInCtrl($state, Auth) {
    var vm = this;
    vm.ctrlName = 'SignInCtrl';

    vm.user = {};

    Auth.currentUser().then(function(user) {
      // User was logged in, or Devise returned
      // previously authenticated session.
      console.log(user); // => {id: 1, ect: '...'}
      vm.user = user;
      vm.loggedin = true;
    }, function(error) {
      // unauthenticated error
      console.log('user not logged in',error);
      vm.message = error.data.error;
    });



    vm.login = function () {
      Auth.login(vm.user)
        .then(function(r,e){
          console.log('login : ',r,e, Auth.isAuthenticated());

          if(Auth.isAuthenticated()){

            $state.go('wolfpack.dash');

          }

        },function(err){
          console.log('failed to logged in',err);
          vm.message = err.data.error;
        });
    };
    vm.logout = function () {
      Auth.logout().then(function(){
        vm.loggedin = false;
      });
    };
  }

})();
