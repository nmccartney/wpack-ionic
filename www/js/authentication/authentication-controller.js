(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name authentication.controller:AuthenticationCtrl
   *
   * @description
   *
   */
  angular
    .module('authentication')
    .controller('AuthenticationCtrl', AuthenticationCtrl);

  function AuthenticationCtrl() {
    var vm = this;
    vm.ctrlName = 'AuthenticationCtrl';
  }

})();
