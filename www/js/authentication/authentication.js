(function () {
  'use strict';

  /* @ngdoc object
   * @name authentication
   * @requires $stateProvider
   *
   * @description
   *
   */
  angular
    .module('authentication', [
      'ui.router'
    ]);

  angular
    .module('authentication')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('authentication', {
        url: '/authentication',
        templateUrl: 'js/authentication/authentication.tpl.html',
        controller: 'AuthenticationCtrl as authentication'
      })
      .state('sign-in', {
        url: '/sign-in',
        templateUrl: 'js/authentication/sign-in.tpl.html',
        controller: 'SignInCtrl as signin'
      })
      .state('sign-up', {
        url: '/sign-up',
        templateUrl: 'js/authentication/sign-up.tpl.html',
        controller: 'SignUpCtrl as signup'
      })
      .state('forgot', {
        url: '/forgot',
        templateUrl: 'js/authentication/forgot-password.tpl.html',
        controller: 'ForgotPasswordCtrl as forgotpassword'
      });
  }

})();
