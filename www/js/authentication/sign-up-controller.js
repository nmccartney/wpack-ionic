(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name authentication.controller:SignUpCtrl
   *
   * @description
   *
   */
  angular
    .module('authentication')
    .controller('SignUpCtrl', SignUpCtrl);

  function SignUpCtrl($state, Auth) {
    var vm = this;
    vm.ctrlName = 'SignUpCtrl';

    vm.user = {};
    vm.errors = {};

    vm.signUp = function(){

      console.log('Signing up ', vm.user);

      Auth.register(vm.user).then(function(registeredUser) {

        console.log(registeredUser); // => {id: 1, ect: '...'}
        vm.confirm = true;

      }, function(error) {

        // Registration failed...
        console.log('failed to register',error.data);
        vm.errors = error.data.errors;
        vm.confirm = false;

      });
    };

    vm.signOutAndIn = function(){
      Auth.logout().then(function(oldUser) {
        // alert(oldUser.name + "you're signed out now.");
        console.log(oldUser.username, 'Logged out.');
        $state.go('sign-in');
      }, function(error) {
        // An error occurred logging out.
        console.log('Error:: ', error);
        $state.go('sign-in');
      });
    };
  }

})();
