(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name pack.directive:packList
     * @restrict EA
     * @element
     *
     * @description
     *
     * @example
     <example module="wolf">
     <file name="index.html">
     <wolf-list></wolf-list>
     </file>
     </example>
     *
     */
    angular
        .module('wolf')
        .directive('wolfList', wolfList);

    function wolfList( WolfApi) {
        return {
            restrict: 'EA',
            scope: {
                wolves:'=',
                selfLoad:'=',
                checklist:'='
            },
            templateUrl: 'js/wolf/wolf-list-directive.tpl.html',
            replace: false,
            controllerAs: 'wolfList',
            controller: function ($scope) {
                var vm = this;
                vm.name = 'wolfList';
                vm.checklist = $scope.checklist;
                vm.wolves = $scope.wolves;

                console.log('checklist', $scope.wolves);

                vm.getWolves = function(){
                    WolfApi.list()
                        .then(function(r){
                            console.log('wolf list - ', r);
                            vm.wolves = r;
                            $scope.$broadcast('wolf.list.loaded');
                        });
                };
            },
            link: function (scope, element, attrs) {
                /*jshint unused:false */

                if(scope.selfLoad){
                    scope.wolfList.getWolves();
                }

                scope.$on('wolf.list.loaded',function(e,r){
                    //console.log('packs loaded', e);
                });

                scope.$on('wolf.list.new',function(e,r){
                    scope.wolfList.wolves.push(r);
                });
            }
        };
    }
})();
