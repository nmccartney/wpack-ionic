/*global describe, beforeEach, it, expect, inject, module*/
'use strict';

describe('WolfApi', function () {
  var factory;

  beforeEach(module('wolfpack'));

  beforeEach(inject(function (WolfApi) {
    factory = WolfApi;
  }));

  it('should have someValue be WolfApi', function () {
    expect(factory.someValue).toEqual('WolfApi');
  });

  it('should have someMethod return WolfApi', function () {
    expect(factory.someMethod()).toEqual('WolfApi');
  });

});
