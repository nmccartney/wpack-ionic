(function () {
    'use strict';

    /**
     * @ngdoc object
     * @name wolf.controller:WolfCtrl
     *
     * @description
     *
     */
    angular
        .module('wolf')
        .controller('WolfViewCtrl', WolfViewCtrl);

    function WolfViewCtrl($state, WolfApi,$ionicLoading, $ionicNavBarDelegate) {
        var vm = this;
        vm.ctrlName = 'WolfViewCtrl';

        $ionicLoading.show({
            template: '<ion-spinner icon="android"></ion-spinner>'
        });

        WolfApi.getWolf({id:$state.params.id})
            .then(function(r){
                $ionicLoading.hide();
                vm.user = r;
                vm.setNavTitle(vm.user.email);
            });

        vm.setNavTitle = function(title) {
            $ionicNavBarDelegate.title(title);
            console.log(title, vm.pack);
            setTimeout(function(){$ionicNavBarDelegate.title(title)},400)
        }
        setTimeout(function(){vm.setNavTitle('Pack')},20);
    }

})();
