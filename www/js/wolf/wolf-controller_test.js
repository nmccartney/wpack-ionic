/*global describe, beforeEach, it, expect, inject, module*/
'use strict';

describe('WolfCtrl', function () {
  var ctrl;

  beforeEach(module('wolf'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('WolfCtrl');
  }));

  it('should have ctrlName as WolfCtrl', function () {
    expect(ctrl.ctrlName).toEqual('WolfCtrl');
  });

});
