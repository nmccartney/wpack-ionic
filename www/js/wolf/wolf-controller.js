(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name wolf.controller:WolfCtrl
   *
   * @description
   *
   */
  angular
    .module('wolf')
    .controller('WolfCtrl', WolfCtrl);

  function WolfCtrl($state, $stateParams, WolfApi) {
    var vm = this;
    vm.ctrlName = 'WolfCtrl';
  }

})();
