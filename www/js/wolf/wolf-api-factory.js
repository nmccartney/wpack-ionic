(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name wolfpack.factory:WolfApi
   *
   * @description
   *
   */
  angular
    .module('wolf')
    .factory('WolfApi', WolfApi);

  function WolfApi($http,$q,$upload) {

    var url = 'http://wolfpack.io';

    var WolfApiBase = {};

    WolfApiBase.someValue = 'WolfApi';

    WolfApiBase.yourWolves = [];
    WolfApiBase.pending = [];
    WolfApiBase.invites = [];
    WolfApiBase.user;

    WolfApiBase.someMethod = function someMethod() {
      return 'WolfApi';
    };

      WolfApiBase.setUser = function (user){
          WolfApiBase.user = user;
          console.log('setting', WolfApiBase.user)
      };

      WolfApiBase.getUser = function (){
          return WolfApiBase.user;
      };

    WolfApiBase.getUserData = function getUserData () {
      var yourwolves = WolfApiBase.getWolves();
      var allwolves = WolfApiBase.findWolves();
      var invites = WolfApiBase.getWolfInvites();
      var pending = WolfApiBase.pendingWolves();

      return $q.all([yourwolves, allwolves, invites, pending])
        .then(function(res){
          WolfApiBase.yourWolves = res[0];
          WolfApiBase.pending = res[3];
          WolfApiBase.invites = res[2];
          return {
            yourwolves : res[0],
            allwolves : res[1],
            invites : res[2],
            pending : res[3]
          };
        });
    };

    /**
     *
     * @param wolf
     * @returns {*|webdriver.promise.Promise}
     */
    WolfApiBase.getWolf = function (wolf) {
        console.log(wolf);
        return $http.get(url + '/user/view/' + wolf.id)
          .then(function(r){
            return r.data;
        });
    };

    /**
     * User looking for current friend list
     * @returns {*|webdriver.promise.Promise}
     */
    WolfApiBase.list = function getWolves () {
      return $http.get(url + '/users/list')
        .then( function ( resp ) { return resp.data; } );
    };

    /**
     * User looking for new friends
     * @returns {*|webdriver.promise.Promise}
     */
    WolfApiBase.findWolves = function findWolves () {
      return $http.get(url + '/users/find.json')
        .then( function ( resp ) { return resp.data; } );

    };

    /**
     * User adding a friend
     * @returns {*|webdriver.promise.Promise}
     */
    WolfApiBase.addWolf = function addWolf ( userId, connectorId, type ) {
      //TODO: create a invite
      return $http.get( url + '/invites/create.json' , {
        params:{
          invitedId: connectorId,
          invitorId: userId,
          type: type
        }
      });
    };

    /**
     * User wanted pending friend requests
     * @returns {*|webdriver.promise.Promise}
     */
    WolfApiBase.pendingWolves = function pendingWolves ( ) {
      return $http.get( url + '/invites/pending.json')
        .then(function (r) {
          return r.data;
        });
    };

    /**
     * User wanted pending friend requests
     * @returns {*|webdriver.promise.Promise}
     */
    WolfApiBase.removePendingWolf = function removePendingWolf ( invite ) {
      return $http.get( url + '/invites/destroy.json',{
          params:{
            id: invite.id
          }
        })
        .then(function (r) {
          return r.data;
        });
    };

    /**
     * User removing a friend
     * @returns {*|webdriver.promise.Promise}
     */
    WolfApiBase.removeWolf = function removeWolf (id) {
      //TODO: remove wolf to wolf association
      return $http.get(url + '/users/remove/' + id);
    };

    /**
     * User wants invite list
     * @returns {*|webdriver.promise.Promise}
     */
    WolfApiBase.getWolfInvites = function getWolfInvites () {
      return $http.get(url + '/invites.json')
        .then(function(r){
          return r.data;
        });
    };

    /**
     * User accepting a friend
     * @returns {*|webdriver.promise.Promise}
     */
    WolfApiBase.acceptWolf = function acceptWolf ( invite ) {
      return $http.get(url+'/invites/confirm/',{params:{id:invite.id}})
        .then(function(result){
          return result.data;
        });
    };

    /**
     * User declining a friend
     * @returns {*|webdriver.promise.Promise}
     */
    WolfApiBase.declineWolf = function declineWolf ( invite ) {
      return $http.get( url + '/invites/destroy',{params:{id:invite.id}} )
        .then( function ( result ) {
          return result.data;
        });
    };

    /**
     *
     * @param wolf
     * @returns {*|webdriver.promise.Promise}
     */
    WolfApiBase.updateWolf = function(wolf){
      return $http.post(url + 'users/update.json',wolf)
        .then(function(result){
          return result.data;
        });
    };

    /**
     *
     * @param wolf
     * @returns {*|webdriver.promise.Promise}
     */
    WolfApiBase.updateWolfAvatar = function(wolf,file){
      return $upload.upload({
        url: 'users/update_avatar/', // upload.php script, node.js route, or servlet url
        data: wolf,
        file: file

      });
    };


    return WolfApiBase;
  }

})();
