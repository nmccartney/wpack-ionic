(function () {
  'use strict';

  /* @ngdoc object
   * @name wolf
   * @requires $stateProvider
   *
   * @description
   *
   */
  angular
    .module('wolf', [
      'ui.router',
      'angularFileUpload'
    ]);

  angular
    .module('wolf')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('admin.wolves', {
        url: '/wolves',
        views:{
            "wolves-tab":{
                templateUrl: 'js/wolf/wolf.tpl.html',
                controller: 'WolfCtrl as wolf'
            }
        }
      })
      .state('admin.wolf-view', {
        url: '/wolves/:id',
        views:{
            "wolves-tab":{
                templateUrl: 'js/wolf/wolf-view.tpl.html',
                controller: 'WolfViewCtrl as wolf'
            }
        }
      });
  }

})();
