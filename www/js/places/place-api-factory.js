(function () {
    'use strict';

    /**
     * @ngdoc service
     * @name pack.factory:PackApi
     *
     * @description
     *
     */
    angular
        .module('place')
        .factory('PlaceApi', PlaceApi);

    function PlaceApi($http,$rootScope) {

        var api = 'http://wolfpack.io';

        var PlaceApiBase = {};

        PlaceApiBase.someValue = 'PlaceApi';

        PlaceApiBase.someMethod = function someMethod() {
            return 'PackApi';
        };


        /* -------- OTHER FUNCTIONS ------ */



        /* ------- BASIC CRUD OPERATIONS ------- */

        /**
         *
         * @param pack
         * @returns {*|webdriver.promise.Promise}
         */
        PlaceApiBase.create = function(place){
            return $http.post(api + '/places/create',  place)
                .then(function(r){
                    $rootScope.$broadcast('pack.place.new',r.data);
                    return r.data;
                });
        };

        /**
         *
         * @param pack
         * @returns {*|webdriver.promise.Promise}
         */
        PlaceApiBase.show = function(place){
            return $http.get(api + '/places/show/'+ place, { id:pack })
                .then(function(r){
                    $rootScope.$broadcast('pack.place.show',r.data);
                    return r.data;
                });
        };

        PlaceApiBase.list = function(pack){
            return $http.get(api + '/places/list',{params:{id:pack}})
                .then(function(r){
                    $rootScope.$broadcast('pack.place.list',r.data);
                    return r.data;
                });
        };

        /**
         *
         * @param pack
         * @returns {*|webdriver.promise.Promise}
         */
        PlaceApiBase.update = function(pack){
            return $http.post(api + '/places/update', pack)
                .then(function(r){
                    $rootScope.$broadcast('pack.place.update',r.data);
                    return r.data;
                });
        };

        /**
         *
         * @param pack
         * @returns {*|webdriver.promise.Promise}
         */
        PlaceApiBase.delete = function(place){
            return $http.delete(api + '/places/destroy' ,{params:{id:place.id}})
                .then(function(r){
                    $rootScope.$broadcast('pack.place.remove',r);
                    return r;
                });
        };


        return PlaceApiBase;
    }

})();
