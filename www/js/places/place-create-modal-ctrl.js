(function () {
    'use strict';

    /**
     * @ngdoc object
     * @name place.controller:PlaceCtrl
     *
     * @description
     *
     */
    angular
        .module('place')
        .controller('PlaceCreateModalCtrl', PlaceCreateModalCtrl);

    function PlaceCreateModalCtrl($scope, PlaceApi,  $ionicLoading) {
        var vm = this;
        vm.ctrlName = 'PlaceCreateModalCtrl';

        vm.createPlace = function($pack){

            var pack = {};
            pack['name'] = $pack.name;
            pack['latitude'] = $pack.geometry.location.G;
            pack['longitude'] = $pack.geometry.location.K;
            pack['pack_id'] = $scope.pack.pack.id;
            pack['pack_name'] = $scope.pack.pack.name;



            $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner>'
            });

            PlaceApi.create(pack)
                .then(function(data){

                    console.log("Pack create :: " , data);

                    vm.event = null;

                    $ionicLoading.hide();
                    //
                    ////TODO: NEEDS TO BE MORE FLEXIBLE??!
                    console.log($scope);
                    if($scope.$parent.packMap)
                        $scope.$parent.packMap.modal.hide();
                    //if($scope.$parent.$parent.user)
                    //    $scope.$parent.$parent.modal.hide();

                    $scope.newPlace = null;
                });
        }
    }

})();
