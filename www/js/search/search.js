(function () {
    'use strict';

    /* @ngdoc object
     * @name search
     * @requires $stateProvider
     *
     * @description
     *
     */
    angular
        .module('search', [ ]);

    angular
        .module('search')
        .config(config);

    function config($stateProvider) {

    }

})();
