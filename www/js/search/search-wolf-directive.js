(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name search.directive:searchWolf
     * @restrict EA
     * @element
     *
     * @description
     *
     * @example
     <example module="search">
     <file name="index.html">
     <search-wolf></search-wolf>
     </file>
     </example>
     *
     */
    angular
        .module('search')
        .directive('searchWolf', searchWolf);

    function searchWolf() {
        return {
            restrict: 'EA',
            scope: {},
            templateUrl: 'js/search/search-wolf-directive.tpl.html',
            replace: false,
            controllerAs: 'searchWolf',
            controller: function ($scope) {
                var vm = this;
                vm.name = 'searchWolf';

                //vm.getPacks = function(){
                //    PackApi.list()
                //        .then(function(r){
                //            console.log('pack list - ', r);
                //            vm.packs = r;
                //            $scope.$broadcast('pack.list.loaded');
                //        });
                //};
            },
            link: function (scope, element, attrs) {
                /*jshint unused:false */

                //scope.packList.getPacks();
                //
                //scope.$on('pack.list.loaded',function(e,r){
                //    //console.log('packs loaded', e);
                //});
                //
                //scope.$on('pack.list.new',function(e,r){
                //    scope.packList.packs.push(r);
                //});
            }
        };
    }

})();
