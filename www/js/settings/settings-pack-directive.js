(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name pack.directive:packList
     * @restrict EA
     * @element
     *
     * @description
     *
     * @example
     <example module="wolf">
     <file name="index.html">
     <event-board></event-board>
     </file>
     </example>
     *
     */
    angular
        .module('settings')
        .directive('settingsPack', settingsPack);

    function settingsPack( $ionicLoading ) {
        return {
            restrict: 'EA',
            scope: {
                pack: '='
            },
            templateUrl: 'js/settings/settings-pack-directive.tpl.html',
            replace: false,
            controllerAs: 'settingsPack',
            controller: function ($scope,$rootScope, $state,PackApi) {
                var vm = this;
                vm.name = 'settingsPack';
                vm.cached = false;
                vm.pack = $scope.pack;

                console.log('settingsPack room');


                vm.loadMap = function(){

                    $ionicLoading.show({
                        template: '<ion-spinner icon="android"></ion-spinner>'
                    });

                    setTimeout(function(){
                        $ionicLoading.hide();
                    },300);
                }

                vm.deletePack = function(pack){

                    $ionicLoading.show({
                        template: '<ion-spinner icon="android"></ion-spinner>'
                    });

                    PackApi.delete(pack)
                        .then(function(e){
                            console.log('deleteing : ' , e);
                            $ionicLoading.hide();
                            $rootScope.$broadcast('pack.deleted',e);
                            $state.go('admin.packs');
                        });
                }
            },
            link: function (scope, element, attrs) {
                /*jshint unused:false */



                scope.$on('pack.settings.load',function(e,r){
                    console.log('settingsPack loaded', e);

                    if(!scope.settingsPack.cached){
                        //scope.settingsPack.loadChat();
                        scope.settingsPack.cached = true;
                    }

                    scope.settingsPack.loadMap();
                });

            }
        };
    }

})();
