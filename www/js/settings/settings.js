(function () {
    'use strict';

    /* @ngdoc object
     * @name settings
     * @requires $stateProvider
     *
     * @description
     *
     */
    angular
        .module('settings', [ ]);

    angular
        .module('settings')
        .config(config);

    function config($stateProvider) {

    }

})();
