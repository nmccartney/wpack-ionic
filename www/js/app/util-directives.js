(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name pack.directive:packList
     * @restrict EA
     * @element
     *
     * @description
     *
     * @example
     <example module="pack">
     <file name="index.html">
     <full-height></full-height>
     </file>
     </example>
     *
     */
    angular
        .module('wolfpack')


        .run(function(){

            $.fn.animNumTo = function(from,to,dur,step,cb){
                var $elem = "<div></div>";
                if(!dur)dur = 600;
                $($elem).prop('Counter',from).animate({
                    Counter: to
                }, {
                    duration: dur,
                    easing: 'linear',
                    step: function (now) {
                        step(now);
                    },
                    complete:cb
                });
            }


        })

        .filter('cut', function () {
            return function (value, wordwise, max, tail) {
                if (!value) return '';

                max = parseInt(max, 10);
                if (!max) return value;
                if (value.length <= max) return value;

                value = value.substr(0, max);
                if (wordwise) {
                    var lastspace = value.lastIndexOf(' ');
                    if (lastspace != -1) {
                        value = value.substr(0, lastspace);
                    }
                }

                return value + (tail);
            };
        })


        .directive('fullHeight', fullHeight);

    function fullHeight() {
        return {
            restrict: 'A',
            //scope:{
            //    fullHeight:'=',
            //    offset:'='
            //},
            link: function (scope, element, attrs) {
                /*jshint unused:false */

                var winHeight = $(window).outerHeight();
                var topNav = 94;
                var botNav = 48;

                //ment for initial ui offset
                var fullHeight = Number(attrs.fullHeight);
                if(!fullHeight)fullHeight = topNav+botNav;
                //ment for additional offsetting from load
                var offset = Number(attrs.offset);
                if(!offset)offset=0;

                //console.log(attrs)



                var offsetting = function(os){
                    offset = Number(os);
                    resize();
                }

                attrs.$observe('offset',offsetting);

                function resize(){

                    //console.log($(element).find('.angular-google-map-container').outerHeight())

                    winHeight = $(window).outerHeight();

                    //console.log('winHeight',winHeight);
                    //console.log('fullheight',fullHeight)
                    //console.log('offset',offset)
                    //console.log('should be ',winHeight - (offset+fullHeight));
                    //console.log('')
                    //console.log('------------')
                    //console.log('')

                    if($(element).find('.angular-google-map-container').outerHeight()==null){
                        $(element).outerHeight(winHeight - (offset+fullHeight))
                    }else{
                        $(element).find('.angular-google-map-container').outerHeight(winHeight - (offset+fullHeight))
                    }
                }

                resize();

                $(window).on('resize',resize);
            }
        };
    }


    angular
        .module('wolfpack')
        .directive('fullWidth', fullWidth);

    function fullWidth(){
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                /*jshint unused:false */

                var winWidth = $(window).outerWidth();
                var offset = 0;

                function resize(){
                    //console.log(winWidth);
                    //console.log($(element).outerWidth())

                    winWidth = $(window).outerWidth();

                    $(element).outerWidth(winWidth - offset );
                }

                resize();

                $(window).on('resize',resize);
            }
        };
    }

    angular
        .module('wolfpack')
    .directive('standardTimeMeridian', function() {
        return {
            restrict: 'AE',
            replace: true,
            scope: {
                etime: '=etime'
            },
            template: "<strong>{{stime}}</strong>",
            link: function(scope, elem, attrs) {

                scope.stime = epochParser(scope.etime, 'time');

                function prependZero(param) {
                    if (String(param).length < 2) {
                        return "0" + String(param);
                    }
                    return param;
                }

                function epochParser(val, opType) {
                    if (val === null) {
                        return "00:00";
                    } else {
                        var meridian = ['AM', 'PM'];

                        if (opType === 'time') {
                            var hours = parseInt(val / 3600);
                            var minutes = (val / 60) % 60;
                            var hoursRes = hours > 12 ? (hours - 12) : hours;

                            var currentMeridian = meridian[parseInt(hours / 12)];

                            return (prependZero(hoursRes) + ":" + prependZero(minutes) + " " + currentMeridian);
                        }
                    }
                }

                scope.$watch('etime', function(newValue, oldValue) {
                    scope.stime = epochParser(scope.etime, 'time');
                });

            }
        };
    })

})();
