(function () {
    'use strict';

    /**
     * @ngdoc object
     * @name pack.controller:AdminDashCtrl
     *
     * @description
     *
     */
    angular
        .module('wolfpack')
        .controller('AdminDashCtrl', DashCtrl);

    function DashCtrl($scope, $auth,$state,$ionicModal,$ionicSideMenuDelegate,WolfApi) {

        $ionicModal.fromTemplateUrl('js/pack/pack-create-modal.tpl.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.openModal = function() {
            $scope.modal.show();
        };

        $scope.closeModal = function() {
            $scope.modal.hide();
        };

        $scope.signout = function(){
            $auth.signOut()
                .then(function(resp) {
                    // handle success response
                    $state.go('index')
                })
                .catch(function(resp) {
                    // handle error response
                });
        }

        $scope.toggleLeft = function() {
            $ionicSideMenuDelegate.toggleLeft();
        };
        $scope.toggleRight = function() {
            $ionicSideMenuDelegate.toggleRight();
        };


        WolfApi.getWolfInvites()
            .then(function(d){
                console.log('invites:',d);
                $scope.invites = d.invites
            })



        $scope.accept = function(invite){
            WolfApi.acceptWolf(invite)
                .then(function(d){
                    console.log('accepted',d);
                    var id = _.indexOf($scope.invites, d);
                    if(id)$scope.invites.splice(id,1);
                })
        }

        $scope.decline = function(invite){
            WolfApi.declineWolf(invite)
                .then(function(d){
                    console.log('declined',d);
                    var id = _.indexOf($scope.invites, d);
                    if(id)$scope.invites.splice(id,1);
                })
        }


    }
})();
