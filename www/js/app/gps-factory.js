(function () {
    'use strict';

    /**
     * @ngdoc service
     * @name chat.factory:ChatApi
     *
     * @description
     *
     */
    angular
        .module('wolfpack')
        .factory('Gps', GpsApi);

    function GpsApi($rootScope) {

        var GpsApiBase = {};
        GpsApiBase.refreshIntervalId;

        GpsApiBase.gps ={};

        GpsApiBase.someValue = 'GpsApi';

        GpsApiBase.someMethod = function someMethod() {
            return 'GpsApi';
        };

        GpsApiBase.get = function() {
            return GpsApiBase.gps;
        };

        GpsApiBase.set = function(gps) {
            $rootScope.$broadcast('user.gps.update',gps);
            //console.log('setting gps');
            return GpsApiBase.gps = gps;
        };

        GpsApiBase.getCurrent = function() {
            return navigator.geolocation.getCurrentPosition(GetLocation);
        };

        /**
         *
         * @param location
         * @constructor
         */
        function GetLocation(location) {
            var gps = {
                latitude : location.coords.latitude,
                longitude : location.coords.longitude,
                accuracy : location.coords.accuracy
            };
            console.log('got gps');
            $rootScope.$broadcast('user.gps.started',gps);
            GpsApiBase.set(gps);
        }

        /**
         * GET DISTANCE BETWEEN 2 POINTS
         * @param newGps
         * @param OldGps
         * @returns {number}
         */
        GpsApiBase.getDistance = function(newGps,OldGps){
            var R = 6371; // km
            var dLat = (newGps.latitude - OldGps.latitude).toRad();
            var dLon = (newGps.longitude - OldGps.longitude).toRad();
            var lat1 = OldGps.latitude.toRad();
            var lat2 = newGps.latitude.toRad();

            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c;
            return d;
        }

        /**
         * START TRACKING CURRENT USER
         */
        GpsApiBase.startTracking = function(){

            var oldLat,oldLng;

            //make sure this isnt already running
            GpsApiBase.stopTracking();

            GpsApiBase.refreshIntervalId = setInterval(function(){



                var gps = GpsApiBase.get();

                //console.log('tracking gps ', gps);

                //make sure we have gps at all
                if(gps.accuracy) {

                    //set up a way to track old from new
                    if (!oldLat && !oldLng) {
                        oldLat = gps.latitude;
                        oldLng = gps.longitude;
                        return;
                    }
                    //get distance
                    var dist = GpsApiBase.getDistance(gps, {latitude:oldLat,longitude:oldLng});

                    //when its worth setting..
                    if(dist > 10){
                        oldLat = gps.latitude;
                        oldLng = gps.longitude;

                        GpsApiBase.set(gps);
                    }

                }else{
                    GpsApiBase.getCurrent();
                }

            }, 5000);
        }

        /**
         * STOP TRACKING CURRENT USER
         */
        GpsApiBase.stopTracking = function(){
            clearInterval(GpsApiBase.refreshIntervalId);
        }

        //GpsApiBase.getCurrent();

        return GpsApiBase;
    }

    Number.prototype.toRad = function() { return this * (Math.PI / 180); };

})();
