// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('wolfpack', [
    'ionic',
    'ui.router',
    'ng-token-auth',
    'btford.socket-io',
    'authentication',
    'pack',
    'wolf',
    'events',
    'search','uiGmapgoogle-maps',
    'ionic-timepicker',
    'ionic-datepicker'
])

.constant('api', 'http://wolfpack.io')


/**
 * SET UP IONIC STUFF
 * @param $ionicPlatform
 */
angular.module('wolfpack').run(ionicSetup);
function ionicSetup($ionicPlatform){
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
}



/**
 * CONFIG TOKEN AUTH
 * @param $authProvider
 */
angular.module('wolfpack').config(tokenAuthConfig);
function tokenAuthConfig($authProvider){
    $authProvider.configure({
        apiUrl:                  'http://wolfpack.io',
        storage:                 'localStorage',
        tokenValidationPath:     '/auth/validate_token',
        signOutUrl:              '/auth/sign_out',
        emailRegistrationPath:   '/auth',
        accountUpdatePath:       '/auth',
        accountDeletePath:       '/auth',
        confirmationSuccessUrl:  'http://wolfpack.io',
        passwordResetPath:       '/auth/password',
        passwordUpdatePath:      '/auth/password',
        passwordResetSuccessUrl: 'http://wolfpack.io',
        emailSignInPath:         '/auth/sign_in',
        proxyIf:                 function() { return false; },
        proxyUrl:                '/proxy',
        authProviderPaths: {
            //github:   '/auth/github',
            //facebook: '/auth/facebook',
            //google:   '/auth/google'
        },
        tokenFormat: {
            "access-token": "{{ token }}",
            "token-type":   "Bearer",
            "client":       "{{ clientId }}",
            "expiry":       "{{ expiry }}",
            "uid":          "{{ uid }}"
        },
        parseExpiry: function(headers) {
            // convert from UTC ruby (seconds) to UTC js (milliseconds)
            //console.log('expiry: ', headers);
            return (parseInt(headers['expiry']) * 1000) || null;
        },
        handleLoginResponse: function(response) {
            //console.log('login resp:',response)
            return response.data;
        },
        handleAccountResponse: function(response) {
            //console.log('account resp:',response)
            return response.data;
        },
        handleTokenValidationResponse: function(response) {
            //console.log('token validation resp:',response);
            localStorage.setItem("wp-user", JSON.stringify(response.data))
            //WolfApi.setUserData(response.data)
            return response.data;
        }
    });
}


/**
 * SAVE USER IN LOCAL STORAGE FOR EASIER ACCESS
 * @param WolfApi
 */
angular.module('wolfpack').run(setLocalStorage);
function setLocalStorage(WolfApi){
    WolfApi.setUser(JSON.parse(localStorage.getItem("wp-user")))
}


/**
 * APPLICATION ROUTING CONFIG
 * @param $stateProvider
 * @param $urlRouterProvider
 * @param $httpProvider
 * @param $ionicConfigProvider
 */
angular.module('wolfpack').config(routeConfig);
function routeConfig($stateProvider,$urlRouterProvider,$httpProvider,$ionicConfigProvider){
    $ionicConfigProvider.tabs.position('bottom');

    //$httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];


    $urlRouterProvider.otherwise("/");

    $stateProvider
        // this state will be visible to everyone
        .state('index', {
            url: '/',
            templateUrl: 'login.html',
            controller: 'IndexCtrl'
        })

        // only authenticated users will be able to see routes that are
        // children of this state
        .state('admin', {
            url: '/admin',
            abstract: true,
            //template: '<ui-view/>',
            templateUrl: 'js/app/admin.tpl.html',
            resolve: {
                auth: function($auth,SocketApi) {
                    console.log('authentication...');
                    return $auth.validateUser()
                        .then(function(d){
                            var user = d;
                            SocketApi.socket.emit('connectserver', user);
                            return d;
                        });
                }
            }
        })

        // this route will only be available to authenticated users
        .state('admin.dashboard', {
            url: '/dash',
            views:{
                'dash-tab':{
                    templateUrl: 'dash.html',
                    controller: 'AdminDashCtrl'
                }
            }
        });
}


angular.module('wolfpack').run(startGps);
function startGps($rootScope,Gps){
    var dereg = $rootScope.$on('user.gps.started',function(){
        dereg();
        console.log('set gps');
        Gps.startTracking();
    });
    //console.log('start gps');
    Gps.getCurrent();
}

/**
 * WE TRY TO  CONNECT ON THE VALIDATION OF A USER
 * AUTHENTICATION EMITS A EVENT TO CONNECT TO SERVER
 * THEN WE LISTEN FOR THE CONNECTION HERE
 * ON CONNECTION WE SUBSCRIBE TO ALL PACKS/ROOMS
 * @param $rootScope
 * @param SocketApi
 * @param WolfApi
 * @param PackApi
 */
angular.module('wolfpack').run(connectSocketServer);
function connectSocketServer($rootScope,SocketApi,WolfApi,PackApi,Gps){
    SocketApi.addSocketListener('ready',$rootScope);
    var dereg = $rootScope.$on('socket.ready',function(ev,data){
        //console.log('ready user id: ', WolfApi.getUser().uid);
        //console.log('ready socket id: ', data.clientId);
        SocketApi.id = data.clientId;

        subscribeToPackRooms();
        //dereg();
    });


    function subscribeToPackRooms(){
        PackApi.list(WolfApi.getUser())
            .then(function(d){
                angular.forEach(d, function(val,id){
                    SocketApi.socket.emit('subscribe',{room:val.name});

                    SocketApi.socket.emit('pack.user.gps.set', { user: WolfApi.getUser(),room:val.name, gps: Gps.get()});
                })
            });
    }
}
