(function () {
    'use strict';

    /**
     * @ngdoc object
     * @name pack.controller:AdminDashCtrl
     *
     * @description
     *
     */
    angular
        .module('wolfpack')
        .controller('IndexCtrl', IndexCtrl);

    function IndexCtrl($scope, $auth,$state,$ionicLoading) {

        $scope.alerts = [];

        console.log('headers : ', $auth.retrieveData('auth_headers'))

        $scope.handleRegBtnClick = function() {
            $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner>'
            });
            console.log($scope.registrationForm)
            $auth.submitRegistration($scope.registrationForm)
                .then(function(resp) {
                    // handle success response
                    $ionicLoading.hide();
                    //console.log('handle success response');
                })
                .catch(function(resp) {
                    // handle error response
                    $ionicLoading.hide();
                    $scope.alerts.push(resp);
                    alert('handle error response')
                    console.log('handle error response');
                });
        };


        $scope.submitLogin = function(){
            $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner>'
            });
            $auth.submitLogin($scope.loginForm)
                .then(function(resp) {
                    // handle success response
                    $ionicLoading.hide();
                    //console.log('handle success response');
                })
                .catch(function(resp) {
                    // handle error response
                    $ionicLoading.hide();
                    $scope.alerts.push(resp)
                    alert('handle error response')
                    console.log('handle error response');
                });
        }


        $auth.validateUser().then(function(resp) {
            // handle success
            setTimeout(function(){
                $ionicLoading.hide();
                $state.go('admin.dashboard')
            },400);

        }).catch(function(resp){
            setTimeout(function(){
                $ionicLoading.hide();
            },200);
            console.log('catch - ', resp);
            console.log('headers : ', $auth.retrieveData('auth_headers'))
        })

        $scope.$on('auth:login-success', function(ev, reason) {
            $state.go('admin.dashboard')
        });

        $scope.$on('auth:login-error', function(ev, reason) {
            $ionicLoading.hide();
            console.log('auth:login-error', reason);
            $scope.alerts = reason.errors;
        });

        $ionicLoading.show({
            template: '<ion-spinner icon="android"></ion-spinner>'
        });

    }

})();
