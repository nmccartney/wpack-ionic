(function () {
    'use strict';

    angular
        .module('wolfpack')
        .factory('SocketApi', SocketApi);

    function SocketApi(socketFactory,$rootScope) {

        var SocketApiBase = {};

        SocketApiBase.someValue = 'SocketApi';

        SocketApiBase.rooms = [];

        SocketApiBase.addRoom = function(room){
            SocketApiBase.rooms.push(room);
        }

        SocketApiBase.deleteRoom = function(room){
            var id = _.indexOf(SocketApiBase.rooms, room);
            if(id!=-1)SocketApiBase.rooms.splice(id,1);
        }

        var myIoSocket = io.connect('wolfpack.io:3700/');

        SocketApiBase.socket = socketFactory({
            prefix:"socket.",
            ioSocket: myIoSocket
        });

        SocketApiBase.addSocketListener = function(evt,scope){
            SocketApiBase.socket.forward(evt, scope);
        };

        return SocketApiBase;
    }

})();
