(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name pack.directive:packList
     * @restrict EA
     * @element
     *
     * @description
     *
     * @example
     <example module="wolf">
     <file name="index.html">
     <event-action></event-action>
     </file>
     </example>
     *
     */
    angular
        .module('pack')
        .directive('eventAction', eventAction);

    function eventAction( ) {
        return {
            restrict: 'EA',
            scope: {
                event:'=',
                users:'='
            },
            templateUrl: 'js/events/event-action-directive.tpl.html',
            replace: false,
            controllerAs: 'eventAction',
            controller: function ($scope, $ionicModal, EventApi,WolfApi) {
                var vm = this;
                vm.name = 'eventBoard';
                vm.status;
                vm.user = WolfApi.getUser();

                if(_.where($scope.users,{id:vm.user.id}).length>=1){
                    vm.status = _.where($scope.users,{id:vm.user.id})[0].status
                }
                //console.log('included: ',vm.status);

                var checkStatus = function(){
                    switch (vm.status){
                        case 'going':
                            vm.rsvp--;
                            break;
                        case 'declined':
                            vm.declined--;
                            break;
                        case 'maybe':
                            vm.maybe--;
                            break;
                        default:
                            vm.status = null;
                            break;
                    }
                }

                vm.accept = function(id){
                    EventApi.status({status:'going',id:id})
                        .then(function(e){
                            //console.log('good to go',e);
                            checkStatus();
                            vm.status = 'going';
                            vm.rsvp++;
                        });
                }
                vm.decline = function(id){
                    EventApi.status({status:'declined',id:id})
                        .then(function(e){
                            //console.log('good to decline',e);
                            checkStatus();
                            vm.status = 'declined';
                            vm.declined++;
                        });
                }
                vm.not_sure = function(id){
                    EventApi.status({status:'maybe',id:id})
                        .then(function(e){
                            //console.log('good to maybe',e);
                            checkStatus();
                            vm.status = 'maybe';
                            vm.maybe++;
                        });
                }

            },
            link: function (scope, element, attrs) {
                /*jshint unused:false */

                //console.log('.....',scope.users);
                //console.log('.....', _.where(scope.users,{'status':'going'}).length);

                scope.eventAction.rsvp = _.where(scope.users,{status:'going'}).length
                scope.eventAction.declined = _.where(scope.users,{status:'declined'}).length
                scope.eventAction.maybe = _.where(scope.users,{status:'maybe'}).length

                scope.$on('event.status.update',function(e,r){
                    console.log('event loaded', e);

                });

            }
        };
    }

})();
