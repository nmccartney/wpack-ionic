(function () {
    'use strict';

    /**
     * @ngdoc object
     * @name pack.controller:PackCtrl
     *
     * @description
     *
     */
    angular
        .module('pack')
        .controller('EventCreateModalCtrl', EventCreateModalCtrl);

    function EventCreateModalCtrl($scope, EventApi,  $ionicLoading) {
        var vm = this;
        vm.ctrlName = 'EventCreateModalCtrl';
        vm.event = {};

        vm.createEvent = function(pack,event){

            $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner>'
            });

            event['pack_id'] = pack.id
            event['pack'] = pack.name;

            EventApi.create(event)
                .then(function(data){

                    console.log("Event create :: " , data);

                    vm.event = null;

                    $ionicLoading.hide();

                    $scope.eventBoard.modal.hide()
                });
        }
    }

})();
