(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name pack.directive:packList
     * @restrict EA
     * @element
     *
     * @description
     *
     * @example
     <example module="wolf">
     <file name="index.html">
     <event-board></event-board>
     </file>
     </example>
     *
     */
    angular
        .module('pack')
        .directive('eventBoard', eventBoard);

    function eventBoard( $ionicLoading, EventApi) {
        return {
            restrict: 'EA',
            scope: {
                pack:'=',
                wolf:'=',
                typeName:'='
            },
            templateUrl: 'js/events/event-board-directive.tpl.html',
            replace: false,
            controllerAs: 'eventBoard',
            controller: function ($scope, $ionicModal, EventApi) {
                var vm = this;
                vm.name = 'eventBoard';
                vm.cached = false;
                vm.pack = $scope.pack;
                vm.wolf = $scope.wolf;
                vm.typeName = $scope.typeName;

                console.log('eventBoard room');

                vm.getEvents = function(pack){


                    $ionicLoading.show({
                        template: '<ion-spinner icon="android"></ion-spinner>'
                    });

                    EventApi.list(pack, 'pack')
                        .then(function(data){
                            console.log('events loaded : ',data);
                            vm.events = data;
                            $ionicLoading.hide();
                        })
                }

                $ionicModal.fromTemplateUrl('js/events/event-create-modal.tpl.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                }).then(function(modal) {
                    vm.modal = modal;
                });

                vm.launchEventCreate = function() {
                    vm.modal.show();
                };

                vm.closeEventCreateModal = function() {
                    vm.modal.hide();
                };


            },
            link: function (scope, element, attrs) {
                /*jshint unused:false */

                scope.$on('pack.events.load',function(e,r){
                    console.log('event loaded', e);

                    if(!scope.eventBoard.cached){
                        //scope.eventBoard.loadChat();
                        scope.eventBoard.cached = true;
                    }

                    //if(!scope.eventBoard.cached)
                    scope.eventBoard.getEvents(scope.pack.pack);
                });

                scope.$on('pack.events.new',function(e,r){
                    scope.eventBoard.events.push(r);
                });

                scope.$on('pack.events.remove',function(e,r){
                    var id = _.indexOf(scope.eventBoard.events, r);
                    console.log('removing event ', id);
                    if(id)scope.eventBoard.events.splice(id,1);
                });

            }
        };
    }

})();
