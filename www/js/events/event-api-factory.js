(function () {
    'use strict';

    /**
     * @ngdoc service
     * @name event.factory:EventApi
     *
     * @description
     *
     */
    angular
        .module('events')
        .factory('EventApi', EventApi);

    function EventApi($http,$rootScope) {

        var api = 'http://wolfpack.io';
        var dev_api = 'http://localhost:3005';

        var EventApiBase = {};

        EventApiBase.someValue = 'EventApi';

        EventApiBase.someMethod = function someMethod() {
            return 'EventApi';
        };


        /* -------- OTHER FUNCTIONS ------ */

        EventApiBase.status = function(status){
            return $http.post(api + '/events/status',  status)
                .then(function(r){
                    return r.data;
                });
        };

        /* ------- BASIC CRUD OPERATIONS ------- */

        /**
         *
         * @param event
         * @returns {*|webdriver.promise.Promise}
         * @expects event =
         *  {
         *      pack_id:id,
         *      pack_name:name,
         *      start_date:sd,
         *      end_date:ed,
         *      start_time:st,
         *      end_time:et,
         *      name:n,
         *      description:d
         *  }
         */
        EventApiBase.create = function(event){
            return $http.post(api + '/events/create',  event)
                .then(function(r){
                    $rootScope.$broadcast('pack.events.new',r.data);
                    return r.data;
                });
        };

        /**
         *
         * @param event
         * @returns {*|webdriver.promise.Promise}
         */
        EventApiBase.show = function(event){
            return $http.get(api + '/events/show/', { id:event.id })
                .then(function(r){
                    return r.data;
                });
        };

        EventApiBase.list = function(obj,type){
            //console.log(obj.id);
            return $http.get(api + '/events/list.json', {params:{id:obj.id,type:type}})
                .then(function(r){
                    return r.data.events;
                });
        };

        /**
         *
         * @param pack
         * @returns {*|webdriver.promise.Promise}
         */
        EventApiBase.update = function(event){
            delete event['url']
            delete event['places']
            delete event['users']
            delete event['$$hashKey']
            delete event['method']
            delete event['pack']
            delete event['pack_id']
            delete event['creator']
            console.log('updateing',event)
            return $http.get(api + '/events/update',{params:{id:event.id, event:event}})
                .then(function(r){
                    $rootScope.$broadcast('pack.events.event.update',r.data);
                    return r.data;
                });
        };

        /**
         *
         * @param pack
         * @returns {*|webdriver.promise.Promise}
         */
        EventApiBase.delete = function(pack){
            return $http.delete(api + '/events/destroy?id=' + pack.id)
                .then(function(r){
                    $rootScope.$broadcast('pack.events.remove',r.data);
                    return r.data;
                });
        };


        return EventApiBase;
    }

})();
