(function () {
    'use strict';

    /**
     * @ngdoc object
     * @name pack.controller:PackCtrl
     *
     * @description
     *
     */
    angular
        .module('pack')
        .controller('EventUpdateModalCtrl', EventUpdateModalCtrl);

    function EventUpdateModalCtrl($scope, EventApi,  $ionicLoading) {
        var vm = this;
        vm.ctrlName = 'EventUpdateModalCtrl';

        $scope.currentDate = new Date();
        $scope.title = "Custom Title";
        $scope.newEvent;

        console.log($scope)
        //vm.event = {};

        vm.setEvent = function(event){
            $scope.newEvent = event;
            console.log('setting event',event)
        }

        vm.cancelEvent = function(event){

            $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner>'
            });

            event['pack_id'] = event.pack.id
            event['pack'] = event.pack.name;

            EventApi.delete(event)
                .then(function(){
                    vm.event = null;
                    $ionicLoading.hide();
                    $scope.eventCard.modal.hide()
                })
        }

        vm.updateEvent = function(event){

            $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner>'
            });

            event['pack_id'] = event.pack.id
            event['pack'] = event.pack.name;

            EventApi.update(event)
                .then(function(data){

                    console.log("Event create :: " , data);

                    vm.event = null;

                    $ionicLoading.hide();

                    $scope.eventCard.modal.hide()
                });
        }

        $scope.slots = {epochTime: 12600, format: 12, step: 15};

        $scope.startTime = function (val) {
            if (typeof (val) === 'undefined') {
                console.log('Time not selected');
            } else {
                console.log('Selected time is : ', val);    // `val` will contain the selected time in epoch
                $scope.eventCard.event.start_time = val;
            }
        };

        $scope.endTime = function (val) {
            if (typeof (val) === 'undefined') {
                console.log('Time not selected');
            } else {
                console.log('Selected time is : ', val);    // `val` will contain the selected time in epoch
                $scope.eventCard.event.end_time = val;
            }
        };

        $scope.startDate = function (val) {
            if(typeof(val)==='undefined'){
                console.log('Date not selected');
            }else{
                console.log('Selected date is : ', val);
                console.log('Selected date is : ', vm.event);
                $scope.eventCard.event.start_date = val;
            }
        };

        $scope.endDate = function (val) {
            if(typeof(val)==='undefined'){
                console.log('Date not selected');
            }else{
                console.log('Selected date is : ', val);
                $scope.eventCard.event.end_date = val;
            }
        };
    }


})();
