(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name pack.directive:packList
     * @restrict EA
     * @element
     *
     * @description
     *
     * @example
     <example module="wolf">
     <file name="index.html">
     <event-action></event-action>
     </file>
     </example>
     *
     */
    angular
        .module('pack')
        .directive('eventCard', eventCard);

    function eventCard( ) {
        return {
            restrict: 'EA',
            //scope: {
            //    event:'=',
            //    users:'='
            //},
            //templateUrl: 'js/events/event-action-directive.tpl.html',
            replace: false,
            controllerAs: 'eventCard',
            controller: function ($scope, $ionicModal, EventApi,WolfApi) {
                var vm = this;
                vm.name = 'eventCard';
                vm.user = WolfApi.getUser();

                vm.openEdit = function(event){
                    //console.log('included: ',vm.user);
                    vm.event = event;
                    $ionicModal.fromTemplateUrl('js/events/event-update-modal.tpl.html', {
                        scope: $scope,
                        animation: 'slide-in-up'
                    }).then(function(modal) {
                        //console.log('modal: ',vm.modal);
                        vm.modal = modal;
                        vm.modal.show();

                    });
                }

                vm.closeEdit = function(){
                    vm.modal.hide();
                    vm.event = null;
                    vm.modal = null;
                }
            },
            link: function (scope, element, attrs) {
                /*jshint unused:false */

                //console.log('.....',scope);
                //console.log('.....', _.where(scope.users,{'status':'going'}).length);


            }
        };
    }

})();
