(function () {
    'use strict';

    /* @ngdoc object
     * @name events
     * @requires $stateProvider
     *
     * @description
     *
     */
    angular
        .module('events', [ ]);

    angular
        .module('events')
        .config(config);

    function config($stateProvider) {

    }

})();
