(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name pack.directive:packList
     * @restrict EA
     * @element
     *
     * @description
     *
     * @example
     <example module="wolf">
     <file name="index.html">
     <event-action></event-action>
     </file>
     </example>
     *
     */
    angular
        .module('pack')
        .directive('mapPlaceView', mapPlaceView);

    function mapPlaceView( ) {
        return {
            restrict: 'EA',
            scope: {
                place:'=',
                close:'='
            },
            templateUrl: 'js/map/map-place-view-directive.tpl.html',
            replace: false,
            controllerAs: 'mapPlaceView',
            controller: function ($scope, $ionicModal, EventApi,WolfApi) {
                var vm = this;
                vm.name = 'mapPlaceView';
                vm.user = WolfApi.getUser();
            },
            link: function (scope, element, attrs) {
                /*jshint unused:false */
                scope.closePanel = scope.close;
                //console.log('.....',scope);
                //console.log('.....', _.where(scope.users,{'status':'going'}).length);


            }
        };
    }

})();
