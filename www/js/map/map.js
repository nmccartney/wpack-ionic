(function () {
    'use strict';

    /* @ngdoc object
     * @name map
     * @requires $stateProvider
     *
     * @description
     *
     */
    angular
        .module('map', ['google.places','uiGmapgoogle-maps', 'place' ]);

    angular
        .module('map')
        .config(config);

    function config($stateProvider) {

    }

})();
