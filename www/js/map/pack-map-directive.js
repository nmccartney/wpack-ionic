(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name pack.directive:packList
     * @restrict EA
     * @element
     *
     * @description
     *
     * @example
     <example module="wolf">
     <file name="index.html">
     <event-board></event-board>
     </file>
     </example>
     *
     */
    angular
        .module('map')
        .directive('packMap', packMap);

    function packMap( $ionicLoading,uiGmapGoogleMapApi) {
        return {
            restrict: 'EA',
            scope: {
                pack:'=pack'
            },
            templateUrl: 'js/map/pack-map-directive.tpl.html',
            replace: false,
            controllerAs: 'packMap',
            controller: function ($scope,$ionicModal, PlaceApi,uiGmapGoogleMapApi,uiGmapIsReady,WolfApi) {
                var vm = this;
                vm.name = 'packMap';
                vm.cached = false;
                vm.pack = $scope.pack;
                vm.places = [];
                vm.users = [];
                vm.user = WolfApi.getUser();

                console.log('packMap room',vm.pack);

                vm.map = {
                    center: { latitude: 40.2, longitude: -80 },
                    zoom: 8 ,
                    refresh: true,
                    options: {
                        disableDefaultUI: true
                    },
                    events: {},
                    bounds: {},
                    polys: [],
                    draw: undefined
                };

                uiGmapGoogleMapApi.then(function(maps) {
                    vm.gMaps = maps;
                    //console.log(maps);
                });
                uiGmapIsReady.promise(1).then(function(instances) {
                    instances.forEach(function(inst) {
                        vm.googleMap = inst.map;
                        vm.uuid = vm.googleMap.uiGmap_id;
                        vm.mapInstanceNumber = inst.instance; // Starts at 1.
                    });
                    //console.log('maps : ',vm.googleMap);

                    $('.angular-google-map-container').addClass('set');

                    vm.gMaps.event.trigger(vm.googleMap, 'resize');
                });

                vm.loadMap = function(){

                    $ionicLoading.show({
                        template: '<ion-spinner icon="android"></ion-spinner>'
                    });

                    console.log('id',vm.pack)

                    PlaceApi.list(vm.pack.id)
                        .then(function(d){
                            //console.log('Places : ' , d);
                            vm.places = d;
                            $ionicLoading.hide();
                        });
                }

            },
            link: function (scope, element, attrs) {
                /*jshint unused:false */

                scope.fit = true;
                scope.panelSize = 0;
                scope.selectedPlace;


                scope.group = {show:false};

                scope.toggleGroup = function(group) {
                    group.show = !group.show;
                };
                scope.isGroupShown = function(group) {
                    return group.show;
                };

                scope.$on('pack.map.load',function(e,r){
                    console.log('map loaded', e);

                    if(!scope.packMap.cached){
                        //scope.mapPage.loadChat();
                        scope.packMap.cached = true;
                    }
                });

                scope.$on('pack.loaded',function(evt,pack){
                    scope.packMap.pack = pack;
                    scope.packMap.loadMap();
                });

                scope.$on('pack.place.new',function(e,r){
                    //    TODO: ADD NEW PLACE TO PLACE LIST
                    scope.packMap.places.push(r);
                });

                scope.$on('pack.place.remove',function(e,r){
                    //    TODO: remove PLACE TO PLACE LIST
                    console.log('pack.place.remove',r);
                });

                scope.$on('pack.user.new',function(e,r){
                    //    TODO: ADD NEW USER TO PLACE LIST
                });

                scope.$on('pack.user.remove',function(e,r){
                    //    TODO: remove USER TO PLACE LIST
                });

                scope.$on('pack.user.position.update',function(e,r){
                    //    TODO: ADD NEW USER TO PLACE LIST
                });

                scope.placeClick = function(marker,event,obj){
                    scope.fit = false;
                    scope.panelSize = 300;
                    scope.selectedPlace = obj;
                    //scope.centerMap(obj);
                    $.fn.animNumTo(
                        0,
                        300,
                        400,
                        function(n){
                            scope.packMap.gMaps.event.trigger(scope.packMap.googleMap, 'resize');
                        },
                        function(){
                            scope.centerMap(scope.selectedPlace);
                            scope.packMap.map.zoom = 19;
                        });

                }

                scope.closePanel = function(){
                    scope.panelSize = 0;

                    $.fn.animNumTo(
                        0,
                        300,
                        400,
                        function(n){
                            scope.packMap.gMaps.event.trigger(scope.packMap.googleMap, 'resize');
                        },
                        function(){//complete:
                            scope.fitMapBounds();
                            scope.fit = true;
                            scope.selectedPlace = null;
                        });
                }

                scope.fitMapBounds = function(){
                    var bounds = new scope.packMap.gMaps.LatLngBounds();
                    for (var i = 0; i < scope.packMap.places.length; i++) {
                        bounds.extend({
                            lat:function(){return scope.packMap.places[i].latitude},
                            lng:function(){return scope.packMap.places[i].longitude}
                        });
                    }

                    scope.packMap.googleMap.setCenter(bounds.getCenter());
                    scope.packMap.googleMap.fitBounds(bounds);
                }

                scope.centerMap = function(obj){
                    if(!obj && scope.selectedPlace)obj = scope.selectedPlace;
                    scope.packMap.map.center = { latitude: Number(obj.latitude) - 0, longitude: Number(obj.longitude) + 0};
                }

            }
        };
    }

})();
