

(function () {

    angular
        .module('wolfpack')
        .directive('mapNav', mapNav);

    function mapNav($ionicModal){
        return {
            restrict: 'E',
            scope:{
                pack:'=pack',
                users:'=users',
                user:'=user',
                map: '=map',
                gmap: '=gmap'
            },
            templateUrl:"js/map/map-nav-directive.tpl.html",
            controllerAs: 'mapNav',
            controller:function($scope,$ionicModal){

                var vm = this;
                vm.name = 'mapNav';

                vm.openModal = function() {
                    $ionicModal.fromTemplateUrl('js/places/place-create-modal.tpl.html', {
                        scope: $scope,
                        animation: 'slide-in-up'
                    }).then(function(modal) {
                        vm.modal = modal;
                        vm.modal.show();
                    });
                };

                vm.closeModal = function() {
                    vm.modal.hide();
                    vm.modal = null;
                };

                vm.deletePlace = function(place,id){
                    PlaceApi.delete(place)
                        .then(function(e){
                            console.log('place deleted ::' ,e);

                            vm.places.splice(id,1);
                        });
                }

                vm.centerUser = function(){
                    var bounds = new $scope.gmap.LatLngBounds();

                    console.log($scope.user);
                    console.log($scope.map);

                    if(!$scope.user.latitude)return;


                    bounds.extend({
                        lat:function(){return $scope.user.latitude},
                        lng:function(){return $scope.user.longitude}
                    });

                    $scope.map.setCenter(bounds.getCenter());
                    $scope.map.setZoom(19);
                }
            },
            link: function (scope, element, attrs,$ionicModal,PlaceApi) {
                /*jshint unused:false */

                element.on('click', toggle);

                function toggle() {
                    element.toggleClass('open');
                }

                scope.closeModal = scope.mapNav.closeModal;

            }
        };
    }


})();
