(function () {
    'use strict';

    /**
     * @ngdoc service
     * @name map.factory:MapApi
     *
     * @description
     *
     */
    angular
        .module('map')
        .factory('MapApi', MapApi);

    function MapApi($http) {

        var api = 'http://wolfpack.io';

        var MapApiBase = {};

        MapApiBase.someValue = 'MapApi';

        MapApiBase.someMethod = function someMethod() {
            return 'MapApi';
        };


        /* -------- OTHER FUNCTIONS ------ */



        /* ------- BASIC CRUD OPERATIONS ------- */

        /**
         *
         * @param pack
         * @returns {*|webdriver.promise.Promise}
         */
        MapApiBase.create = function(pack, wolf){
            return $http.post(api + '/packs/create.json',  {
                pack:pack,
                wolf:wolf
            })
                .then(function(r){
                    return r.data;
                });
        };

        /**
         *
         * @param pack
         * @returns {*|webdriver.promise.Promise}
         */
        MapApiBase.show = function(pack){
            return $http.get(api + '/packs/show/'+ pack, { id:pack })
                .then(function(r){
                    return r.data;
                });
        };

        MapApiBase.list = function(wolf){
            return $http.get(api + '/packs/', wolf)
                .then(function(r){
                    return r.data;
                });
        };

        /**
         *
         * @param pack
         * @returns {*|webdriver.promise.Promise}
         */
        MapApiBase.update = function(pack){
            return $http.post(api + '/packs/update', pack)
                .then(function(r){
                    return r.data;
                });
        };

        /**
         *
         * @param pack
         * @returns {*|webdriver.promise.Promise}
         */
        MapApiBase.delete = function(pack){
            return $http.delete(api + '/packs/destroy?id=' + pack.id)
                .then(function(r){
                    return r.data;
                });
        };


        return MapApiBase;
    }

})();
