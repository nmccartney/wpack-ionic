

(function () {

    angular
        .module('wolfpack')
        .directive('mapUsers', mapUsers);

    function mapUsers($rootScope,WolfApi,SocketApi,Gps){
        return {
            restrict: 'AE',
            scope: {
                users: '=users',
                pack: '=pack'
            },
            link: function (scope, element, attrs) {
                /*jshint unused:false */
                scope.user = WolfApi.getUser();

                var deregLoaded = $rootScope.$on('pack.loaded',onPackLoaded);
                var degregRoomClients;

                function onPackLoaded(pack){
                    console.log('on pack loaded',pack);
                    SocketApi.socket.emit('pack.get.users', { room:pack.name});

                    deregLoaded();

                    console.log('setup listeners');
                    //listen for this user gps to be set
                    $rootScope.$on('user.gps.update',gpsUpdate);
                    SocketApi.addSocketListener('pack.user.gps.set',scope);
                    scope.$on('socket.pack.user.gps.set',userGpsUpdate);
                    SocketApi.addSocketListener('presence',scope);
                    scope.$on('socket.presence',userPresence);
                    SocketApi.addSocketListener('roomclients',scope);
                    degregRoomClients = scope.$on('socket.roomclients',getUsersFromRoom);

                //
                //    SocketApi.socket.emit('pack.user.gps.set', { user: scope.user,room:pack.name, gps: Gps.get()});
                }


                function gpsUpdate(d,r){
                    if(!scope.user)return;
                    //console.log('user ::: ', scope.user);
                    console.log('gps ::: ', r);

                    scope.user['latitude'] = r.latitude;
                    scope.user['longitude'] = r.longitude;
                    scope.user['accuracy'] = r.accuracy;

                    console.log('send gps signal',scope.pack);
                    SocketApi.socket.emit('pack.user.gps.set', { user: scope.user,room:scope.pack.name, gps: r});
                }


                function userGpsUpdate(evt,data){
                    console.log('socket set user gps for', data.user.uid);

                    var user ={};
                    user['user'] = data.user.uid;
                    var id = _.findIndex(scope.users,{uid:user.user});

                    //console.log(JSON.stringify(scope.users) + ' / ' + JSON.stringify(user), id);

                    //if user doesnt exist we cant update them;
                    if(id == -1){
                        id = scope.users.push(data.user) -1;
                    }

                    user = scope.users[id];

                    user['latitude'] = data.gps.latitude;
                    user['longitude'] = data.gps.longitude;
                    user['accuracy'] = data.gps.accuracy;
                }


                function userPresence(evt,data){
                    console.log('socket user presence for ', data.user);

                    //make sure this event is for this pack
                    if(data.room == scope.pack.name){

                        var user = data.user;
                        var id = _.findIndex(scope.users,{uid:user.user});

                        console.log('user dups : ', id);

                        if(data.state == 'online' && id == -1){
                            //console.log('adding user ', id);
                            scope.users.push(user);
                            //scope.packMap.users = scope.users;

                        }else if(data.state == 'offline'){
                            //console.log('removing user ', id);
                            if(id)scope.users.splice(id,1);
                        }

                    }

                }



                function getUsersFromRoom(evt,data){
                    console.log('roomclients :: ', data);
                    //pack sure pack is loaded
                    //check if this event is for this pack
                    if(data.room == scope.pack.name){

                        angular.forEach(data.users,function(user,id){

                            if(user.latitude!=undefined)scope.users.push(user);

                        });
                        //stop listening
                        degregRoomClients();
                    }

                }

            }
        };
    }


})();
