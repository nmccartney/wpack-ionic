(function () {
    'use strict';

    /**
     * @ngdoc service
     * @name chat.factory:ChatApi
     *
     * @description
     *
     */
    angular
        .module('pack')
        .factory('ChatApi', ChatApi);

    function ChatApi($http) {

        var api = 'http://wolfpack.io';

        var ChatApiBase = {};

        ChatApiBase.someValue = 'ChatApi';

        ChatApiBase.someMethod = function someMethod() {
            return 'ChatApi';
        };


        /* -------- OTHER FUNCTIONS ------ */



        /* ------- BASIC CRUD OPERATIONS ------- */

        /**
         *
         * @param pack
         * @returns {*|webdriver.promise.Promise}
         */
        ChatApiBase.create = function(id, message){
            return $http.post(api + '/messages/create',  {
                id:id,
                message:message
            })
                .then(function(r){
                    return r.data;
                });
        };

        /**
         *
         * @param pack
         * @returns {*|webdriver.promise.Promise}
         */
        ChatApiBase.show = function(pack){
            return $http.get(api + '/messages/show/'+ pack, { id:pack })
                .then(function(r){
                    return r.data;
                });
        };

        ChatApiBase.list = function(id){
            return $http.get(api + '/messages/list', {params:{id:id}})
                .then(function(r){
                    return r.data;
                });
        };

        /**
         *
         * @param pack
         * @returns {*|webdriver.promise.Promise}
         */
        ChatApiBase.update = function(pack){
            return $http.post(api + '/messages/update', pack)
                .then(function(r){
                    return r.data;
                });
        };

        /**
         *
         * @param pack
         * @returns {*|webdriver.promise.Promise}
         */
        ChatApiBase.delete = function(pack){
            return $http.delete(api + '/messages/destroy?id=' + pack.id)
                .then(function(r){
                    return r.data;
                });
        };


        return ChatApiBase;
    }

})();
