(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name pack.directive:packList
     * @restrict EA
     * @element
     *
     * @description
     *
     * @example
     <example module="wolf">
     <file name="index.html">
     <chat-room></chat-room>
     </file>
     </example>
     *
     */
    angular
        .module('pack')
        .directive('chatRoom', chatRoom);

    function chatRoom( WolfApi,$ionicLoading,$ionicScrollDelegate) {
        return {
            restrict: 'EA',
            scope: {
                pack:'='
            },
            templateUrl: 'js/chat/chat-room-directive.tpl.html',
            replace: false,
            controllerAs: 'chatRoom',
            controller: function ($scope,$ionicScrollDelegate,ChatApi,SocketApi) {
                var vm = this;
                vm.name = 'chatRoom';
                vm.cached = false;
                vm.pack = $scope.pack;
                vm.data = {};
                vm.myId = '12345';
                vm.messages = [];
                vm.user = WolfApi.getUser();

                //console.log('looking ', vm.user)

                //console.log('chat room');

                vm.loadChat = function(){

                    $ionicLoading.show({
                        template: '<ion-spinner icon="android"></ion-spinner>'
                    });

                    console.log('load Chat',vm.pack.pack);

                    ChatApi.list(vm.pack.pack.message_board.id)
                        .then(function(e){
                            console.log('list ' ,e)
                            vm.messages = e;
                            $ionicLoading.hide();
                            $ionicScrollDelegate.$getByHandle('mainScroll').scrollBottom();
                        })
                }



                //chat

                vm.hideTime = true;

                var alternate,
                    isIOS = ionic.Platform.isWebView() && ionic.Platform.isIOS();

                vm.sendMessage = function() {
                    alternate = !alternate;

                    //SocketApi.socket.emit('chatmessage', { message: vm.data.message, room: vm.pack.pack.name });


                    ChatApi.create(vm.pack.pack.message_board.id, vm.data.message)
                        .then(function (d) {
                            console.log('create ' ,d);

                            vm.messages.push(d);

                            delete vm.data.message;

                            //$ionicScrollDelegate.scrollBottom(true);
                            $ionicScrollDelegate.$getByHandle('mainScroll').scrollBottom();


                            SocketApi.socket.emit('chatmessage', { message: d, room: vm.pack.pack.name });
                        })
                };

                SocketApi.addSocketListener('chatmessage',$scope);
                $scope.$on('chatmessage',function(evt,data){
                    console.log('socket user message', data)
                    vm.messages.push(data.message);
                    $ionicScrollDelegate.$getByHandle('mainScroll').scrollBottom();
                });


                vm.inputUp = function() {
                    if (isIOS) vm.data.keyboardHeight = 216;
                    $timeout(function() {
                        $ionicScrollDelegate.scrollBottom(true);
                    }, 300);

                };

                vm.inputDown = function() {
                    if (isIOS) vm.data.keyboardHeight = 0;
                    $ionicScrollDelegate.resize();
                };

                vm.closeKeyboard = function() {
                    // cordova.plugins.Keyboard.close();
                };

            },
            link: function (scope, element, attrs, $timeout, $ionicScrollDelegate) {
                /*jshint unused:false */

                scope.$on('pack.chat.load',function(e,r){
                    console.log('chat loaded', e);

                    if(!scope.chatRoom.cached){
                        //scope.chatRoom.loadChat();
                        scope.chatRoom.cached = true;
                    }

                    scope.chatRoom.loadChat();



                });




            }
        };
    }

})();
