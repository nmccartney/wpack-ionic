(function () {
    'use strict';

    /* @ngdoc object
     * @name chat
     * @requires $stateProvider
     *
     * @description
     *
     */
    angular
        .module('chat', [ ]);

    angular
        .module('chat')
        .config(config);

    function config($stateProvider) {

    }

})();
